﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace assignment_ni_hero_da.Utils
{
    public class DotEnv
    {
        public static Dictionary<string, string> Load()
        {
            string path = "../../../.env";
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("No .env file discovered in project directory.");
            }

            Dictionary<string, string> data = new();

            using (StreamReader sr = new StreamReader(path))
            {
                string line;
                while((line = sr.ReadLine()) != null)
                {
                    if(line.IndexOf('=') == -1)
                    {
                        throw new InvalidDataException("Formatting of found .env file was invalid. Line was missing '='");
                    }

                    string key = line.Split('=')[0].Trim();
                    string value = line.Split('=')[1].Trim();
                    data.Add(key, value);
                }

                return data;
            }

        }
    }
}
