INSERT INTO Superhero (Name, Alias, Origin) 
VALUES ('Monkey D. Luffy', 'Mugiwara', 'Captain of the Strawhats pirates with a bounty over 1 billion berry'),
('Asta', 'Shrimpsta', '3rd Class Junior Magic Knight of the Black Bulls'),
('Saitama', 'Caped Baldy', 'Hero for fun');