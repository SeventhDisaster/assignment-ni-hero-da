﻿ALTER TABLE Assistant 
	ADD SuperheroId int not null
	CONSTRAINT SuperheroAssistant
	FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id)
