﻿CREATE TABLE SuperheroPower (
	SuperheroId INT NOT NULL FOREIGN KEY REFERENCES Superhero(Id),
	PowerId INT NOT NULL FOREIGN KEY REFERENCES Power(Id)
	PRIMARY KEY (SuperheroId, PowerId)
)