﻿INSERT INTO Power (Name, Description) 
VALUES ('Rubber-Rubber Fruit', 'Has the ability to stretch all parts of the persons body'),
('Anti-Magic', 'Nullifies all magic'),
('One Punch', 'Literally defeats everything in One Punch, except mosquitoes'),
('Normal Punch', 'Just a arm stretching out fast, you know, normal'),
('Serious Side Steps', 'Jumping to the sides, really, really fast. Thats it');

INSERT INTO SuperheroPower (SuperheroId, PowerId)
VALUES (1, 1), 
(2, 2), 
(3, 3), 
(1, 4), 
(2, 4), 
(3, 4), 
(3, 5);