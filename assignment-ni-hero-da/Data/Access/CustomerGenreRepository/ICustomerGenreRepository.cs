﻿using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_ni_hero_da.Data.Access
{
    interface ICustomerGenreRepository
    {
        SqlConnection Connect();

        CustomerGenre[] GetCustomerGenres(int? customerId);

    }
}
