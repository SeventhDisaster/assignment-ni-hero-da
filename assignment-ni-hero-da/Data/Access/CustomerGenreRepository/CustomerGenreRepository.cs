﻿using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;


namespace assignment_ni_hero_da.Data.Access
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Connects to the databse using the DotEnv utils file reading
        /// </summary>
        /// <returns>A database connection</returns>
        public SqlConnection Connect()
        {
            SqlConnection connection = new(ConnectionHelper.GetConnectionString());
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Reads from database and deserializes a customergenre result
        /// </summary>
        /// <param name="query">SQL statement</param>
        /// <returns>A list of customergenre results from query</returns>
        private CustomerGenre[] ReadCustomerGenres(string query)
        {
            using (SqlCommand command = new(query, Connect()))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    List<CustomerGenre> list = new();
                    while (reader.Read())
                    {
                        list.Add(
                            new CustomerGenre(
                                        reader.GetInt32(0),
                                        reader.GetString(1),
                                        reader.GetString(2),
                                        reader.GetString(3),
                                        reader.GetInt32(4)
                                )
                        );
                    }
                    return list.ToArray();
                }
            }
        }

        /// <summary>
        /// Searches the database for a provided user Id's favorite genres based on invoice
        /// </summary>
        /// <param name="customerId">Id of a customer in database</param>
        /// <returns>Array of CustomerGenre</returns>
        public CustomerGenre[] GetCustomerGenres(int? customerId)
        {
            if(!customerId.HasValue)
            {
                throw new ArgumentNullException("Cannot find favorites of user ID null");
            }
            string query = "SELECT Id, FirstName, LastName, Genre, TrackCount " +
                "FROM(SELECT Customer.CustomerId AS Id, Customer.FirstName AS FirstName, Customer.LastName AS LastName, Genre.Name AS Genre, COUNT(Genre.GenreId) AS TrackCount " +
                    "FROM Customer " +
                    "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                    "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                    "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId " +
                    "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                    $"WHERE Invoice.CustomerId = {customerId} " +
                    "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name " +
                ") UserGenres " +
                "WHERE TrackCount = ( " +
                    "SELECT MAX(Amount) " +
                    "FROM( " +
                        "SELECT Customer.FirstName AS FirstName, Customer.LastName AS LastName, Genre.Name AS Genre, COUNT(Genre.GenreId) AS Amount " +
                        "FROM Customer " +
                        "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                        "INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                        "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId " +
                        "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                        $"WHERE Invoice.CustomerId = {customerId}" +
                        "GROUP BY Customer.FirstName, Customer.LastName, Genre.Name " +
                    ") Counted )";

            return ReadCustomerGenres(query);
        }
    }
}
