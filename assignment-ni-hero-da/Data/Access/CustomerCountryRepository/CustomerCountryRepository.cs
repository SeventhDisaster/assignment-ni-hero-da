﻿using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace assignment_ni_hero_da.Data.Access
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// Creates ConnectionString to read and write to DB depending on Repository functionality
        /// </summary>
        /// <returns>Returns the connection</returns>
        public SqlConnection Connect()
        {
            SqlConnection connection = new(ConnectionHelper.GetConnectionString());
            connection.Open();
            return connection;
        }
        /// <summary>
        /// Reads in all the data returned from the query listed below <see cref="GetCustomerCountries"/> with its query string.
        /// </summary>
        /// <param name="query">Handed in query for returning specific data from DB</param>
        /// <returns>Returns an array of CustomerCountry classes</returns>
        private CustomerCountry[] ReadCustomerCountries(string query)
        {
            using (SqlCommand command = new(query, Connect()))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    List<CustomerCountry> list = new();
                    while (reader.Read())
                    {
                        list.Add(
                            new CustomerCountry(
                                        reader.GetString(0),
                                        reader.GetInt32(1)
                                )
                        );
                    }
                    return list.ToArray();
                }
            }
        }

        /// <summary>
        /// Creates the DB query and calls the <see cref="ReadCustomerCountries(string)"/>
        /// </summary>
        /// <returns>Returns the array of CustomerCountry from ReadCustomerCountries method</returns>
        public CustomerCountry[] GetCustomerCountries()
        {
            string query = "SELECT Country, COUNT(Country)" +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY COUNT(Country) DESC";

            return ReadCustomerCountries(query);
        }
    }
}
