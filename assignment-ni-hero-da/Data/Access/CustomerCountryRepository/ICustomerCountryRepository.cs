﻿using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;

namespace assignment_ni_hero_da.Data.Access
{
    interface ICustomerCountryRepository
    {
        SqlConnection Connect();

        CustomerCountry[] GetCustomerCountries();
    }
}
