﻿using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;

namespace assignment_ni_hero_da.Data.Access
{
    public interface ICustomerRepository
    {
        SqlConnection Connect();

        Customer[] GetCustomers();

        Customer GetCustomer(int id);

        Customer[] GetCustomersByName(string name);

        Customer[] GetCustomerPage(int limit, int offset);

        int InsertCustomer(Customer customer);
        int UpdateCustomer(Customer customer);
    }
}
