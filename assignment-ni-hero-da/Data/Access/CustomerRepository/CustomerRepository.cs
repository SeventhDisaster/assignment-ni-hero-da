﻿using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;

namespace assignment_ni_hero_da.Data.Access
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Creates ConnectionString to read and write to DB depending on Repository functionality
        /// </summary>
        /// <returns>Returns the connection</returns>
        public SqlConnection Connect()
        {
            SqlConnection connection = new(ConnectionHelper.GetConnectionString());
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Gets all Customer in the DB and the data specified in the Customer class for one specific Customer.
        /// </summary>
        /// <param name="query">Query for fetching all customer and their data</param>
        /// <returns>Returns Array of Customer</returns>
        private Customer[] ReadCustomers(string query)
        {
            using (SqlCommand command = new(query, Connect()))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    List<Customer> list = new();
                    while (reader.Read())
                    {
                        list.Add(
                            new Customer(
                                        reader.GetInt32(0),
                                        reader.GetString(1),
                                        reader.IsDBNull(2) ? null : reader.GetString(2),
                                        reader.IsDBNull(7) ? null : reader.GetString(7),
                                        reader.IsDBNull(8) ? null : reader.GetString(8),
                                        reader.IsDBNull(4) ? null : reader.GetString(4),
                                        reader.IsDBNull(9) ? null : reader.GetString(9),
                                        reader.GetString(11)
                                )
                        );
                    }
                    return list.ToArray();
                }
            }
        }

        /// <summary>
        /// Sets up connection to Insert or Update a Customer.
        /// </summary>
        /// <param name="query">Can be Insert or Update query</param>
        /// <param name="c">Customer class containing the new data to be inserted or updated</param>
        /// <returns>Returns the amount of effected rows</returns>
        private int WriteCustomers(string query, Customer c)
        {
            using (SqlCommand command = new(query, Connect()))
            {
                command.Parameters.AddWithValue("@FirstName", c.FirstName);
                command.Parameters.AddWithValue("@LastName", c.LastName);
                command.Parameters.AddWithValue("@Country", c.Country);
                command.Parameters.AddWithValue("@PostalCode", c.PostalCode);
                command.Parameters.AddWithValue("@Address", c.Address);
                command.Parameters.AddWithValue("@Phone", c.Phone);
                command.Parameters.AddWithValue("@Email", c.Email);

                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Get a single customer from the DB specified by an Id
        /// </summary>
        /// <param name="id">Id of the Customer to fetch in DB</param>
        /// <returns>Returns the customer data</returns>
        public Customer GetCustomer(int id)
        {
            string query = $"SELECT * FROM Customer WHERE CustomerId = {id}";

            return ReadCustomers(query)[0];
        }

        /// <summary>
        /// Gets all Customer overload method
        /// </summary>
        /// <returns>Array of all Customers in the DB</returns>
        public Customer[] GetCustomers()
        {
            string query = "SELECT * FROM Customer";

            return ReadCustomers(query);
        }

        /// <summary>
        /// Select all Customers containing the same name structure handed in in the parameter
        /// </summary>
        /// <param name="name">String containing the name to look for or similiar structure: Ex: "Gunn" will find Gunnar</param>
        /// <returns>All Customers of similiar name structure</returns>
        public Customer[] GetCustomersByName(string name)
        {
            string query = $"SELECT * FROM Customer WHERE FirstName LIKE '{name}%'";

            return ReadCustomers(query);
        }

        /// <summary>
        /// Gets a certain amount of Customers specified in the parameters
        /// </summary>
        /// <param name="limit">Max amount of Customers to get</param>
        /// <param name="offset">From which row to start get Customers from</param>
        /// <returns>Returns all Customers from offset and amount is equal to limit</returns>
        public Customer[] GetCustomerPage(int limit, int offset)
        {
            string query = $"SELECT * FROM Customer " +
                $"ORDER BY CustomerId " +
                $"OFFSET {offset} ROWS " +
                $"FETCH NEXT {limit} ROWS ONLY";
            return ReadCustomers(query);
        }

        /// <summary>
        /// Inserts a new Customer 
        /// </summary>
        /// <param name="customer">New Customer class to insert</param>
        /// <returns>Returns effected rows</returns>
        public int InsertCustomer(Customer customer)
        {
            string query = $"INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Address, Phone, Email)" +
                $"VALUES(" +
                $"@FirstName," +
                $"@LastName," +
                $"@Country," +
                $"@PostalCode," +
                $"@Address," +
                $"@Phone," +
                $"@Email" +
                $")";
            return WriteCustomers(query, customer);
        }
        
        /// <summary>
        /// Update a Customers data
        /// </summary>
        /// <param name="customer">Class containing the new data</param>
        /// <returns>Returns effected rows</returns>
        public int UpdateCustomer(Customer customer)
        {
            string query = $"UPDATE Customer "+
                $"SET " +
                $"FirstName = @FirstName," +
                $"LastName = @LastName," +
                $"Country = @Country," +
                $"PostalCode = @PostalCode," +
                $"Address = @Address," +
                $"Phone = @Phone," +
                $"Email = @Email " +
                $"WHERE CustomerId = {customer.CustomerId}";
            return WriteCustomers(query, customer);
        }
    }
}
