﻿using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using assignment_ni_hero_da.Utils;

namespace assignment_ni_hero_da
{
    public class ConnectionHelper
    {
        /// <summary>
        /// Sets up connection to the Database through SqlConnectionStringBuilder. Uses a custom DotEnv C# file that
        /// paths up to a file reader to read the Datasource for your specific database if puttet in. .env file should be placed
        /// on same level as Data folder.
        /// </summary>
        /// <returns>Returns the ConnectionString for connecting to db when query</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = DotEnv.Load().GetValueOrDefault("DATASOURCE");
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            return connectionStringBuilder.ConnectionString;
        }

    }
}
