﻿using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;


namespace assignment_ni_hero_da.Data.Access
{
    public interface ICustomerSpenderRepository
    {
        SqlConnection Connect();

        CustomerSpender[] GetCustomerSpenders();
    }
}
