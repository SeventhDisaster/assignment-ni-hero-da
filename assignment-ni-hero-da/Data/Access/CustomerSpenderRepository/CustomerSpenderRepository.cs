﻿

using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;

namespace assignment_ni_hero_da.Data.Access
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        /// <summary>
        /// Creates ConnectionString to read and write to DB depending on Repository functionality
        /// </summary>
        /// <returns>Returns the connection</returns>
        public SqlConnection Connect()
        {
            SqlConnection connection = new(ConnectionHelper.GetConnectionString());
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Get the biggest customer spenders registered in the DB by Invoice data
        /// </summary>
        /// <returns>Array of CustomerSpender</returns>
        public CustomerSpender[] GetCustomerSpenders()
        {
            string query = 
                "SELECT Customer.CustomerId, FirstName, LastName, SUM(Invoice.Total) AS TotalSum FROM Customer " +
                "INNER JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                "ORDER BY TotalSum DESC";

            using (SqlCommand command = new SqlCommand(query, Connect()))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    List<CustomerSpender> list = new();
                    while(reader.Read())
                    {
                        list.Add(
                                new CustomerSpender(
                                    reader.GetInt32(0),
                                    reader.GetString(1),
                                    reader.GetString(2),
                                    reader.GetDecimal(3)
                                    )
                            );
                    }
                    return list.ToArray();
                }
            }
        }
    }
}
