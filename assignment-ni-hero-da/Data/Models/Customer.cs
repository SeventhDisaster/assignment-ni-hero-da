﻿
namespace assignment_ni_hero_da.Data.Models
{
    public class Customer
    {
        public int? CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Country { get; set; }
        public string? PostalCode { get; set; }
        public string? Address { get; set; }
        public string? Phone { get; set; }
        public string Email { get; set; }

        public Customer(int? customerId, string firstName, string lastName, string? country, string? postalCode, string? address, string? phone, string email)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            Country = country;
            PostalCode = postalCode;
            Address = address;
            Phone = phone;
            Email = email;
        }

        public override string ToString()
        {
            return $"Id: {CustomerId}\n" +
                $"Full name: {FirstName} {LastName}\n" +
                $"Address: {Address} {Country}, {PostalCode}\n" +
                $"Contact: Phone {Phone} and Email {Email}\n";
        }
    }
}
