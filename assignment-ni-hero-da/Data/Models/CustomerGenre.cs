﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment_ni_hero_da.Data.Models
{
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Genre { get; set; }
        public int TrackCount { get; set; }

        public CustomerGenre(int customerId, string firstName, string lastName, string genre, int trackCount)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            Genre = genre;
            TrackCount = trackCount;
        }

        public override string ToString()
        {
            return $"Customer ID: {CustomerId}\n" +
                $"Name: {FirstName} {LastName} \n" +
                $"Most Popular Genre: {Genre} ({TrackCount} tracks of genre)";
        }
    }
}
