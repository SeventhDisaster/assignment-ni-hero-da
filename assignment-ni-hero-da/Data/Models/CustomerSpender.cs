﻿
namespace assignment_ni_hero_da.Data.Models
{
    public class CustomerSpender
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal TotalSum { get; set; }

        public CustomerSpender(int customerId, string firstName, string lastName, decimal totalSum)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            TotalSum = totalSum;
        }

        public override string ToString()
        {
            return $"Id {CustomerId}: {FirstName} {LastName} has spent {TotalSum}";
        }
    }
}
