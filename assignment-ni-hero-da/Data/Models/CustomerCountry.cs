﻿
namespace assignment_ni_hero_da.Data.Models
{
    public class CustomerCountry
    {
        public string CountryName { get; set; }

        public int Amount { get; set; }

        public CustomerCountry(string countryName, int amount)
        {
            CountryName = countryName;
            Amount = amount;
        }

        public override string ToString()
        {
            return $"{CountryName}({Amount})";
        }
    }
}
