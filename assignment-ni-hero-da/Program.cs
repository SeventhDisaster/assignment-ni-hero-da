﻿using System;
using System.Collections.Generic;
using assignment_ni_hero_da.Data.Access;
using assignment_ni_hero_da.Data.Models;
using Microsoft.Data.SqlClient;

namespace assignment_ni_hero_da
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomerRepository customerRepo = new();
            CustomerCountryRepository customerCountryRepo = new();
            CustomerSpenderRepository customerSpenderRepo = new();
            CustomerGenreRepository customerGenreRepo = new();

            // Get all Customers
            Customer[] customersA = customerRepo.GetCustomers();
            foreach(Customer customer in customersA)
            {
                Console.WriteLine(customer.ToString());
            }

            // Get Customer by Id
            Customer customerA = customerRepo.GetCustomer(3);
            Console.WriteLine(customerA.ToString());
            
            // Get Customers by name
            Customer[] customerMa = customerRepo.GetCustomersByName("Ma");
            foreach(Customer customer in customerMa)
            { // Returns 6 people
                Console.WriteLine(customer.ToString());
            }

            // Get page of customers with limit and offset
            Customer[] customersB = customerRepo.GetCustomerPage(10, 5);
            foreach(Customer customer in customersB)
            { // Returns 10 from index 5
                Console.WriteLine(customer.ToString());
            }

            // Create a new Customer
            Customer customerB = new(null, "Gunnar", "Gunnarson", "Norway", "0555", "Noenstedsplass 5", "+4722222233", "Gunnar@gunnar.gunnar");
            int affectedRows = customerRepo.InsertCustomer(customerB);
            Console.WriteLine(affectedRows);
            Customer[] gunnar = customerRepo.GetCustomersByName("Gunnar");
            Console.WriteLine(gunnar[0].ToString());

            // Update a Customer
            Customer customerSeven = customerRepo.GetCustomer(7);
            Console.WriteLine(customerSeven.ToString());
            customerB.LastName = "Gunnarsson";
            Console.WriteLine($"Affected Rows: {customerRepo.UpdateCustomer(customerSeven)}");
            Customer reCustomerSeven = customerRepo.GetCustomer(7);
            Console.WriteLine(reCustomerSeven.ToString()); // Changed value

            // Get Customer Countries
            CustomerCountry[] countries = customerCountryRepo.GetCustomerCountries();
            foreach(CustomerCountry country in countries)
            {
                Console.WriteLine(country.ToString());
            }

            // Get the most spenders of Customers
            CustomerSpender[] spenders = customerSpenderRepo.GetCustomerSpenders();
            foreach(CustomerSpender spender in spenders)
            {
                Console.WriteLine(spender.ToString());
            }

            //Get a customer's most popular genres
            Customer sampleTwoFaveCustomer = customerRepo.GetCustomer(12);
            CustomerGenre[] favorites = customerGenreRepo.GetCustomerGenres(sampleTwoFaveCustomer.CustomerId);

            foreach (CustomerGenre genre in favorites)
            {
                Console.WriteLine(genre.ToString());
            }

        }
    }
}
