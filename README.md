# Noroff Accelerate .NET Assignment 2 - Data Persistance
### Developer Team:
- Krister (SeventhDisaster) Emanuelsen
- Tommy (Tommyhama95) Hamarsnes

## Appendix A:
The scripts for Appendix A of the assignment can be found under the following path: `/assignment-ni-hero-da/scripts-appendixA/`

## Appendix B:
Our answer for appendix B are found in the rest of the project structure. Most of it under `/Data/` and `/Utils/`, and the methods are run in `Program.cs`.

## Setup:
This application requires an environment variable to be created in order to properly set up a connection.

Create a file under the main project `/assignment-ni-hero-da/` and name it `.env` and then fill it in with:

```
DATASOURCE = MYDATASOURCE
```
For example:
> `DATASOURCE = ND-5CG91961SY\SQLEXPRESS`

Note: the project isn't ACTUALLY set up for .env stuff, But we pretend it is with simple file IO reading under `/Utils/DotEnv.cs` :^)

---

## Future improvements
There are aspects of the project that we would likely intend to improve given more time and effort.

- CustomerGenre SQL statement:
The used SQL statement in `/Data/Access/CustomerGenreRepository.cs` is a behemoth of a complicated statement and could be greatly simplified. For example using `SELECT TOP 1 WITH TIES` (Something we didn't discover until shortly before delivery)
- Avoiding re-writing the same lines of code.
Primarilly the `Connect()` method. We would want to extract the connect method to one separate class so we avoid writing it over. We decided to just keep it since it was short. 
- And ideally we would turn the `Read/WriteObject()` methods into generic methods somewhere to avoid having to re-write the entire connection -> reader/writer -> object deserialization sequence over and over for each object.

---

# Legacy of Gunnar Gunnarsson
There is an epic story about a man named Gunnar Gunnarsson.

The story is in this 36 second youtube video: https://www.youtube.com/watch?v=I-OOpZitfd0 

